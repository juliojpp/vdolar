
package root;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/")
public class MiDolar {
    @Context
    private HttpHeaders headers;
    
    @Context
    HttpServletRequest request;
    
    
  /*  @Path("/fecha/{fecha}")
    @GET 
    public Response Date(@PathParam("fecha") Date date){return null;
}
    @Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    
    public Response getMiDolar(){
        Client client=ClientBuilder.newClient();
       
        
        String dollar = request.getHeader("X-Forwarded-For");
   
        
        String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/2019/09/dias/27/?formato=JSON&apikey=0f74fe963f2638b3f9314a7c4e564dca268919fc";

        Response r = client.target(url).request().get();
        String ValorSbif = r.readEntity(String.class);
        
        return Response.ok("{\"Fuente: SBIF\":"+ValorSbif+"{}").build();
        //return Response.ok("{\"dollar\":"+ dollar +","+ValorSbif+"{problema}").build();
    }
*/

@GET
@Path("/api/{fecha}")
@Produces({MediaType.TEXT_HTML,MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getMiDolar(@PathParam("fecha") String fecha){
        Client client = ClientBuilder.newClient();
		
String url=  "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/2019/09/dias/27/?formato=JSON&apikey=0f74fe963f2638b3f9314a7c4e564dca268919fc";

	Response respuestaExt = client.target(url).request().get();
	String ValorSbif = respuestaExt.readEntity(String.class);
        
	return Response.ok("{\"Fuente: SBIF\":"+ValorSbif+"{}").build();
}
}